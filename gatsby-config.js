module.exports = {
  siteMetadata: {
    siteUrl: "https://isocubes.io",
    title: "isocubes-site",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-typescript`,
      options: {
        isTSX: true, // defaults to false
        jsxPragma: `jsx`, // defaults to "React"
        allExtensions: true, // defaults to false
      },
    },
    {
      resolve: `gatsby-plugin-layout`,
      options: {
        component: require.resolve(`./src/isocube-components/Layout.jsx`),
      },
    },
    {
      resolve: `gatsby-plugin-jss`,
    },
    {
      resolve: `gatsby-plugin-anchor-links`,
    },
    {
      resolve: `gatsby-plugin-breakpoints`,
    },
  ],
};
