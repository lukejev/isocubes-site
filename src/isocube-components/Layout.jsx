import React from 'react'
import Theme from '../component-library/context/Theme'
import SimpleNav from '../component-library/components/SimpleNav/SimpleNav'
import Helmet from 'react-helmet'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faDiscord } from '@fortawesome/free-brands-svg-icons'

import Favicon16 from '../images/favicon-16x16.png'
import Favicon32 from '../images/favicon-32x32.png'
import AppleTouchFavicon from '../images/apple-touch-icon.png'
import SafariFavicon from '../images/safari-pinned-tab.svg'
import PreviewBanner from '../images/previewbanner.png'

import OpenSeaIcon from '../images/opensea-logo.svg'

const Layout = (props) => {

    const links = [
        {
			variant: "link",
			children: "About",
			href: "#about"
		},
        {
			variant: "link",
			children: "Collection",
			href: "#collection"
		},
        {
			variant: "link",
			children: "Community",
			href: "#community"
		}
    ]

    const { 
        children
    } = props

    return (
        <Theme>
            <Helmet>
                <meta charset="utf-8"/>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
                <meta name="robots" content="NOODP"/>
                <meta http-equiv="Cache-Control" content="NO-CACHE"/>

                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
                <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400&display=swap" rel="stylesheet"/>

                <link rel="apple-touch-icon" sizes="76x76" href={AppleTouchFavicon}/>
                <link rel="icon" type="image/png" sizes="32x32" href={Favicon32}/>
                <link rel="icon" type="image/png" sizes="16x16" href={Favicon16}/>
                <link rel="mask-icon" href={SafariFavicon} color="#5bbad5"/>
                <meta name="theme-color" content="#ffffff"/>

                <title>IsoCubes: 8,888 Isometric NFTs</title>
                <meta name="title" content="IsoCubes: 8,888 Isometric NFTs"/>
                <meta name="description" content="IsoCubes is a non-fungible art collection with 8,888 unique generated and hand drawn isometric pieces to collect and trade, built on the Ethereum blockchain."/>
                <meta name="keywords" content="Isocubes, NFT, OpenSea, Mint"/>

                <meta property="og:type" content="website"/>
                <meta property="og:site_name" content="Isocubes"/>
                <meta property="og:url" content="https://isocubes.io/"/>
                <meta property="og:title" content="IsoCubes: 8,888 Isometric NFTs"/>
                <meta property="og:description" content="IsoCubes is a non-fungible art collection with 8,888 unique generated and hand drawn isometric pieces to collect and trade, built on the Ethereum blockchain."/>
                <meta property="og:image" content={PreviewBanner}/>

                <meta property="twitter:card" content="summary_large_image"/>
                <meta name="twitter:creator" content="@isocubesio"></meta>
                <meta property="twitter:url" content="https://isocubes.io/"/>
                <meta property="twitter:title" content="IsoCubes: 8,888 Isometric NFTs"/>
                <meta property="twitter:description" content="IsoCubes is a non-fungible art collection with 8,888 unique generated and hand drawn isometric pieces to collect and trade, built on the Ethereum blockchain."/>
                <meta property="twitter:image" content={PreviewBanner}/>
                <meta name="twitter:image:alt" content="A group of isometric cubes, used to form the logo of IsoCubes"/>
            </Helmet>
            <SimpleNav title="IsoCubes" links={links}>
                <a target='_blank' href="https://twitter.com/isocubesio" style={{ margin: '8px'}}><FontAwesomeIcon icon={ faTwitter } color='white' size='2x' /></a>
                <a target='_blank' href="https://discord.gg/UwqdTh5Trs" style={{ margin: '8px'}}><FontAwesomeIcon icon={ faDiscord } color='white' size='2x' /></a>
                <a target='_blank' href="#" style={{ height: '40px', margin: '8px'}}><img src={OpenSeaIcon} style={{ height: '40px'}} alt="Open sea logo, linking to Isocubes"/></a>
            </SimpleNav>  
            {children}
        </Theme>
    )
}

export default Layout
