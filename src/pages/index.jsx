import * as React from "react"
import { Link } from 'gatsby'

import HeroBanner from "../component-library/components/HeroBanner/HeroBanner"

import Container from "../component-library/components/Container/Container"
import Typography from "../component-library/components/Typography/Typography"
import Image from "../component-library/components/Image/Image"
import Button from "../component-library/components/Button/Button"

import MetaMaskImage from '../images/isocubemmaskicon.png'
import HeroBannerImage from '../images/herobanner.png'

const IndexPage = () => {

  return (
	  <>
		<HeroBanner size='large' alignment="center" backgroundImage={HeroBannerImage}>
			<img src={MetaMaskImage}/>
		</HeroBanner>
		<Container id='about' fullHeight center paddingY={2} paddingX={8}>
			<Typography type='heading'>Here's where the page body content will go.</Typography>
			<Typography>
				<Link to="/Page2">Some more with a test Link component</Link>
				<Button variant='gatsby-link' to='/Page2'>Button with nested Link</Button>
			</Typography>
			<Typography>And some more</Typography>
		</Container>
		<Container id='collection' center altBg paddingY={8} paddingX={8}>
			<Typography type='heading'>Here's where the page body content will go.</Typography>
			<Typography>Some more</Typography>
			<Typography>And some more</Typography>
			<Image height={400} wrapper={true} src='https://i.imgur.com/gQwJCLS.jpg' alt='da kerm' />
		</Container>
		<Container id='community' center altBg paddingY={8} paddingX={8}>
			<Typography type='heading'>Here's where the page body content will go.</Typography>
			<Typography>Some more</Typography>
			<Typography>And some more</Typography>
			<Image height={400} wrapper={true} src='https://i.imgur.com/gQwJCLS.jpg' alt='da kerm' />
		</Container>
	  </>
  )
}

export default IndexPage
