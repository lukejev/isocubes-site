import { createUseStyles, Styles } from 'react-jss'
import { Classes } from 'jss'

import { ITheme } from '../context/Theme'

interface CssRules {
	name: string
}

type StyleObj<T> = Styles<string, T, undefined>
type StyleRules<T> = StyleObj<T> | ((theme: ITheme) => StyleObj<T>)
type ReturnClasses<T> = (data?: (T & { theme?: ITheme | undefined; }) | undefined) => Classes<string>

const makeStyles = <T = Record<string, any>,>(styles: StyleRules<T>, options: CssRules): ReturnClasses<T> => createUseStyles<string, T, ITheme>(styles, options)
export default makeStyles