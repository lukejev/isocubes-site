import { useMemo } from 'react'
import { useTheme } from 'react-jss'
import { ITheme } from '../context/Theme'

const useOffCanvas = (): HTMLDivElement | null => {
	const theme: ITheme = useTheme()
	return useMemo(() => theme.offCanvas, [theme.offCanvas])
}

export default useOffCanvas