import React from 'react'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles((theme: any) => ({
	'@global': {
		body: {
			margin: 0,
			backgroundColor: theme?.palette?.grey?.[100],
		},
		'*': {
			fontFamily: theme.typography.paragraph,
			fontSize: theme.spacing(2)
		},
	}
}))

const GlobalStyles: React.FC<unknown> = () => {
	useStyles()
	return null
}

export default GlobalStyles
