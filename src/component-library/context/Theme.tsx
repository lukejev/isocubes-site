import React, { createRef, useEffect, useState } from 'react'
import { ThemeProvider } from 'react-jss'
import GlobalStyles from './GlobalStyles'

/** TYPES AND INTERFACES */

export interface ITypography {
	heading: string
	paragraph: string
	label: string
	[typeface: string]: string
}

export interface IColor {
	[color: string]: {
		[magnatude: number]: string
	}
}

export interface IThemeColors {
	burn: string
	main: string
	fade: string
	backgroundDark: string
	backgroundMain: string
	backgroundLight: string
	textLight: string
	textMain: string
	textDark: string
	[themeColor: string]: string
}

export interface IThemePalettes {
	primary: IThemeColors
	secondary: IThemeColors
	tertiary: IThemeColors
	[palette: string]: IThemeColors
}

export interface IBreakpoints {
	xs: number
	sm: number
	md: number
	lg: number
	xl: number

	up: (size: string) => string
	down: (size: string) => string
	inside: (firstSize: string, secondSize: string) => string
	is: (size: string) => boolean
	//	outside: (size: string) => string
}

export interface IElevations {
	[magnitude: number]: string
}

export interface IZIndex {
	[zIndex: string]: number
}

type SpacingFn = (...args: number[]) => number | number[]

// type TransitionTimingStepJumpTerms = 'jump-end' | 'jump-start' | 'jump-none' | 'jump-both' | 'start' | 'end'
// type TransitionTimingStepFunction = (steps: number, term?: TransitionTimingStepJumpTerms) => string

// type TransitionTimingQubicBezierFunction = (p1: number, p2: number, p3: number, p4: number) => string

export interface TransitionConfig {
	method: string
	duration: number
	delay?: number
}

export interface ITransition {
	linear: string,
	ease: string,
	easeIn: string,
	easeOut: string,
	easeInOut: string,
	stepStart: string,
	stepEnd: string,

	fast: string,
	medium: string,
	slow: string,
	custom: (config: TransitionConfig) => string
}

export interface ITheme {
	typography: ITypography
	colors: IColor
	palettes: IThemePalettes
	breakpoint: IBreakpoints
	elevation: IElevations
	zIndex: IZIndex
	transition: ITransition
	offCanvas: HTMLDivElement | null

	spacing: SpacingFn
	getPalette: (themeName: string) => IThemeColors
}


/** GLOBAL METHODS */

const createWidthStr = (direction: 'min' | 'max', size: number) => `${direction}-width: ${size}px`
const createMinWidth = (size: number) => createWidthStr('min', size)
const createMaxWidth = (size: number) => createWidthStr('min', size)

const spacing: SpacingFn = (units, ...args) => {
	const px = args.map(item => item * units)
	if (px.length === 1) return px[0]
	return px
}

const getPaletteFrom = (palettes: IThemePalettes) => (themeName: string) => palettes[themeName]

const createTransition = (config: TransitionConfig): string => {
	if (config.delay) return `${config.method} ${config.duration}s ${config.delay}s`
	else return `${config.method} ${config.duration}s`
}


/** DEFAULT THEMES */

const typography: ITypography = {
	heading: "'Bebas Neue', sans-serif",
	paragraph: "'Source Sans Pro', sans-serif",
	label: "'Source Sans Pro', sans-serif",
}

const colors: IColor = {
	white: '#ffffff',
	black: '#000000',
	blue: "#83e6db",
	darkbue: "#05b09a",
	green: "#85ffa5",
	backdrop: {
		0: 'transparent',
		100: '#0000000d',
		200: '#0000001a',
		700: '#00000033',
		900: '#00000099',
	},
	grey: {
		50: "#212521",
		100: '#ece9ea',
		700: '#363032',
		900: '#201d1e',
	},
	opal: {
		100: '#f3f7f6',
		700: '#c6d8d3',
		900: '#92b5ab',
	},
	papaya: {
		100: '#fef8ec',
		700: '#fdf0d5',
		900: '#fadb9e',
	},	
	tart: {
		100: '#f7a4a1',
		700: '#f0544f',
		900: '#ed3731',
	},
	ruby: {
		100: '#eb6f99',
		700: '#d81e5b',
		900: '#b4184c',
	},
	peach: {
		100: '#ffcdc5', 
		300: '#ffb8ad',
		400: '#ffa496;',
		500: '#d1968f',
		600: '#ffaea1',
		700: '#f0a89d',
		900: '#e2998e'
	}
}

const palettes: IThemePalettes = {
	primary: {
		fade: colors.peach[100],
		main: colors.peach[700],
		burn: colors.peach[900],
		textDark: colors.grey[100],
		textMain: colors.grey[100],
		textLight: colors.grey[100],
		backgroundDark: colors.grey[100],
		backgroundMain: colors.grey[50],
		backgroundLight: colors.grey[50],
	},
	secondary: {
		fade: colors.ruby[100],
		main: colors.ruby[700],
		burn: colors.ruby[900],
		textDark: colors.grey[900],
		textMain: colors.grey[100],
		textLight: colors.grey[100],
		backgroundDark: colors.ruby[900],
		backgroundMain: colors.ruby[700],
		backgroundLight: colors.ruby[100],
	},
	tertiary: {
		fade: colors.opal[100],
		main: colors.opal[700],
		burn: colors.opal[900],
		textDark: colors.grey[900],
		textMain: colors.grey[900],
		textLight: colors.grey[100],
		backgroundDark: colors.opal[900],
		backgroundMain: colors.opal[700],
		backgroundLight: colors.opal[100],
	}

}

const elevation: IElevations = {
	0: `0px 0px 0px 1px ${colors.backdrop[200]}`,
	1: `0px 0px 4px 1px ${colors.backdrop[200]}`,
	2: `0px 0px 4px 1px ${colors.backdrop[100]}, 0px 2px 4px 2px ${colors.backdrop[200]}`,
	3: `0px 0px 4px 1px ${colors.backdrop[100]}, 0px 2px 4px 2px ${colors.backdrop[700]}`,
}

const breakpoint: IBreakpoints = {
	xs: 0,
	sm: 480,
	md: 768,
	lg: 960,
	xl: 1400,

	up(size: string) {
		const scale: number = (this as any)[size]
		if (!scale) throw new Error(`No breakpoint: '${size}`)

		return `@media (${createMinWidth(scale)})`
	},

	down(size: string) {
		const scale: number = (this as any)[size]
		if (!scale) throw new Error(`No breakpoint: '${size}`)

		return `@media (${createMaxWidth(scale)})`
	},

	inside(firstSize: string, secondSize: string) {
		const firstScale: number = (this as any)[firstSize]
		const secondScale: number = (this as any)[secondSize]

		if (!firstScale) throw new Error(`No breakpoint: '${firstScale}`)
		if (!secondScale) throw new Error(`No breakpoint: '${secondScale}`)

		const min = createMinWidth(Math.min(firstScale, secondScale))
		const max = createMinWidth(Math.max(firstScale, secondScale))

		return `@media (${min} and ${max})`
	},

	is(size: string) {
		return window.innerWidth < (this as any)[size]
	}

	//	TODO: outside?
}

const zIndex: IZIndex = {
	context: 9,
}

const transition: ITransition = {
	ease: 'ease',
	easeIn: 'ease-in',
	easeOut: 'ease-out',
	easeInOut: 'ease-in-out',
	linear: 'linear',
	stepStart: 'step-start',
	stepEnd: 'step-end',

	custom: createTransition,
	fast: createTransition({ method: 'ease-in-out', duration: .15 }),
	medium: createTransition({ method: 'ease-in-out', duration: .3 }),
	slow: createTransition({ method: 'ease-in-out', duration: .5 }),
}

//	initial colors generated at
//	https://coolors.co/363032-c6d8d3-fdf0d5-f0544f-d81e5b
const Theme: React.FC<{ children: React.ReactNode }> = ({ children }) => {
	const offCanvasRef = createRef<HTMLDivElement>()
	const [offCanvas, setOffCanvas] = useState<HTMLDivElement | null>(null)
	useEffect(() => setOffCanvas(offCanvasRef.current))

	const theme: ITheme = {
		typography: { ...typography },
		colors: { ...colors },
		palettes: { ...palettes },
		elevation: { ...elevation },
		breakpoint: { ...breakpoint },
		zIndex: { ...zIndex },
		transition: { ...transition },

		offCanvas,
		spacing: (...args: number[]) => spacing(8, ...args),
		getPalette: getPaletteFrom(palettes)
	}

	return (
		<ThemeProvider
			theme={theme}
			children={
				<>
					<GlobalStyles />
					{children}
					<div ref={offCanvasRef} className='off-canvas' />
				</>
			}
		/>
	)
}

export default Theme