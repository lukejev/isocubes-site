import React, { createRef, useEffect, useState } from 'react'
import makeStyles from '../../hooks/makeStyles'
import Animation from '../Animation/Animation'

interface styleProps {
	height: number
}

const useStyles = makeStyles<styleProps>({
	base: {
		height: 0,
		overflow: 'hidden'
	},
	active: {
		height: ({ height }) => height,
		padding: '0 0 8px 0'
	},

	inner: {
		padding: 1,
		margin: -1,
	},
}, { name: 'Collapse' })

export interface CollapseProps {
	method?: string
	duration?: number
	delay?: number
	active?: boolean
	children: React.ReactNode
}


const Collpase: React.FC<CollapseProps> = (props) => {
	const {
		active,
		method,
		duration,
		delay,
		children
	} = props

	const [height, setHeight] = useState(0)

	const classes = useStyles({ height })

	const innerRef = createRef<HTMLDivElement>()

	const handleResize = () => {
		if (innerRef.current)
			setHeight(innerRef.current.offsetHeight)
	}

	useEffect(() => {
		handleResize()

		window.addEventListener('resize', handleResize)
		return () => window.removeEventListener('resize', handleResize)
	}, [innerRef.current])

	return (
		<Animation
			method={method}
			duration={duration}
			delay={delay}
			baseClass={classes.base}
			activeClass={classes.active}
			active={active}
		>
			<div className={classes.inner} ref={innerRef}>
				{children}
			</div>
		</Animation>
	)
}

export default Collpase
