import React from 'react'
import clsx from 'clsx'
import { ITheme } from '../../context/Theme'

import Typography from '../Typography/Typography'
import { createUseStyles } from 'react-jss'

interface CardHeaderProps {
	children?: string
	className?: string
	leftContent?: React.ReactNode
	rightContent?: React.ReactNode
}


// interface styleProps { }
const useStyles = createUseStyles<string, Record<string, unknown>, ITheme>(theme => ({
	base: {
		padding: theme.spacing(1, 2),
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'baseline',
	},
	leftContent: {
		marginRight: theme.spacing(1)
	}
}), { name: 'CardHeader' })

const CardHeader: React.FC<CardHeaderProps> = (props) => {
	const {
		children,
		className,
		leftContent,
		rightContent,
	} = props

	const classes = useStyles()

	const classList = clsx(
		classes.base,
		className,
	)

	return (
		<div className={classList}>
			<span>
				{
					leftContent && (
						<span className={classes.leftContent}>
							{leftContent}
						</span>
					)
				}

				<Typography
					type='heading'
					preset={4}
					disableMargin
					component='span'
				>
					{children}
				</Typography>
			</span>

			{
				rightContent && (
					<span>
						{rightContent}
					</span>
				)
			}
		</div>
	)
}

export default CardHeader
