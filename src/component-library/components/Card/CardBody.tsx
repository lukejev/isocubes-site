import clsx from 'clsx'
import React from 'react'
import { createUseStyles } from 'react-jss'
import { ITheme } from '../../context/Theme'

// interface styleProps { }
const useStyles = createUseStyles<string, Record<string, unknown>, ITheme>(theme => ({
	base: {
		padding: theme.spacing(1, 2),
	}
}), { name: 'CardBody' })

export interface CardBodyProps {
	children?: React.ReactNode
	className?: string
}
const CardBody: React.FC<CardBodyProps> = (props) => {
	const {
		children,
		className
	} = props

	const classes = useStyles()

	const classList = clsx(
		classes.base,
		className
	)

	return (
		<div className={classList}>
			{children}
		</div>
	)
}

export default CardBody
