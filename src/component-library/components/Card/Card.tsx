import React from 'react'
import clsx from 'clsx'
import { createUseStyles } from 'react-jss'
import { ITheme } from '../../context/Theme'

interface styleProps {
	elevation: number
}
const useStyles = createUseStyles<string, styleProps, ITheme>(theme => ({
	base: {
		backgroundColor: theme.colors.white,
		borderRadius: theme.spacing(1),
		overflow: 'hidden',
		boxShadow: ({ elevation }) => theme.elevation[elevation],
	},
}), { name: 'Card' })

export interface CardProps {
	elevation: number
	children?: React.ReactNode
	className?: string
}

const Card = React.forwardRef<HTMLDivElement, CardProps>((props, ref) => {
	const {
		children,
		className,
		elevation = 0
	} = props

	const classes = useStyles({ elevation })

	const classList = clsx(
		classes.base,
		className
	)

	return (
		<div ref={ref} className={classList}>
			{children}
		</div>
	)
})

export default Card
