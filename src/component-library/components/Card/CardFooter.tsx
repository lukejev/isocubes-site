import clsx from 'clsx'
import React from 'react'
import { createUseStyles } from 'react-jss'
import { ITheme } from '../../context/Theme'

// interface styleProps { }
const useStyles = createUseStyles<string, Record<string, unknown>, ITheme>(theme => ({
	base: {
		padding: theme.spacing(1, 2),
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
	}
}), { name: 'CardFooter' })

export interface CardFooterProps {
	children?: React.ReactNode
	className?: string
}
const CardFooter: React.FC<CardFooterProps> = (props) => {
	const {
		children,
		className
	} = props

	const classes = useStyles()

	const classList = clsx(
		classes.base,
		className
	)

	return (
		<div className={classList}>
			{children}
		</div>
	)
}

export default CardFooter
