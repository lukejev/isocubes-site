import React, { useMemo } from 'react'
import Button, { ButtonProps } from './Button'

export interface ButtonGroupProps {
	children: React.ReactElement<typeof Button> | React.ReactElement<typeof Button>[]
}

const ButtonGroup: React.FC<ButtonGroupProps> = (props) => {
	const { children } = props

	const childList = useMemo(() => (children as any[]).map<JSX.Element>(
		(child: { props: ButtonProps }, index: number, arr: any[]) => {
			if (index === 0)
				return <Button {...child.props} key={`btn-group-${index}`} group='start' />

			if (index === arr.length - 1)
				return <Button {...child.props} key={`btn-group-${index}`} group='end' />

			return <Button {...child.props} key={`btn-group-${index}`} group='center' />
		}
	), [children])

	return <>{childList}</>
}

export default ButtonGroup
