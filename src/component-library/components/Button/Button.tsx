import clsx from 'clsx'
import React, { LegacyRef } from 'react'
import makeStyles from '../../hooks/makeStyles'
import Typography from '../Typography/Typography'

type ButtonGroupPosition = 'start' | 'end' | 'center'

interface styleProps {
	themeName: string
	borderSize: number
}

const useStyles = makeStyles<styleProps>(theme => ({
	base: {
		border: 'none',
		cursor: 'pointer',
		borderRadius: theme.spacing(1),
		padding: theme.spacing(1.5, 3),
		boxSizing: 'border-box',
		textAlign: 'center',
		userSelect: 'none',
		transition: '1s ease-in-out'
	},

	startGroup: {
		borderRadius: theme.spacing(1, 0, 0, 1),
	},
	endGroup: {
		borderRadius: theme.spacing(0, 1, 1, 0),
	},
	centerGroup: {
		borderRadius: 0
	},

	contained: {
		color: ({ themeName }) => theme.getPalette(themeName).textMain,
		backgroundColor: ({ themeName }) => theme.getPalette(themeName).backgroundMain,

		'&:hover': {
			color: ({ themeName }) => theme.getPalette(themeName).backgroundMain,
			backgroundColor: ({ themeName }) => theme.getPalette(themeName).backgroundDark
		}
	},

	outlined: {
		fontWeight: 600,
		backgroundColor: 'transparent',
		color: ({ themeName }) => theme.getPalette(themeName).textDark,
		border: ({ themeName, borderSize }) => `${borderSize}px solid ${theme.getPalette(themeName).backgroundMain}`,
		margin: ({ borderSize }) => `-${borderSize}px 0`,

		'&:hover': {
			border: ({ themeName, borderSize }) => `${borderSize}px solid ${theme.getPalette(themeName).backgroundDark}`,
		}
	},

	link: {
		backgroundColor: 'transparent',
		padding: [theme.spacing(1, 1), '!important'],
		color: ({ themeName }) => theme.getPalette(themeName).backgroundDark,
		userSelect: 'unset',

		'&:hover': {
			//	grey[900] is a hex colour, so adding '0a' ends up like '#0000000a'
			backgroundColor: theme.colors.grey[900] + '0a',
		}
	},

	large: {
		padding: theme.spacing(2.25, 4),
	},

	small: {
		padding: theme.spacing(1, 2),
	},

	wide: {
		width: '100%'
	}
}), { name: 'Button' })


export interface ButtonProps {
	children: React.ReactNode

	variant?: 'contained' | 'outlined' | 'link'
	theme?: 'primary' | 'secondary'
	type?: string
	size?: 'normal' | 'small' | 'large'
	wide?: boolean
	borderSize?: number
	href?: string
	ref?: React.RefObject<HTMLElement>
	group?: ButtonGroupPosition
	to?: any
	className?: string

	onClick?: (e: React.MouseEvent<any, any>) => void
}

const Button = React.forwardRef<HTMLElement, ButtonProps>((props, ref) => {
	const {
		variant = 'contained',
		theme = 'primary',
		size = 'normal',
		borderSize = 2,
		wide = false,
		group,
		className: externalClasses,
		href,
		onClick,
		to,
		children,
	} = props

	const classes = useStyles({ themeName: theme, borderSize })

	const handleClick = (e: React.MouseEvent<any, any>) => {
		e.preventDefault()

		if (href) location.href = href // TODO: history.push
		else onClick?.(e)
	}

	const className = clsx(
		classes.base,
		variant === 'contained' && classes.contained,
		variant === 'outlined' && classes.outlined,
		variant === 'link' && classes.link,

		size === 'large' && classes.large,
		size === 'small' && classes.small,

		group === 'start' && classes.startGroup,
		group === 'center' && classes.centerGroup,
		group === 'end' && classes.endGroup,

		wide && classes.wide,

		externalClasses
	)

	const labelPreset = (
		size === 'large' ?
			2 : (
				size === 'small' ?
					4 : 3
			)
	)

	const buttonProps = {
		className,
		onClick: handleClick
	}

	const child = (
		typeof children === 'string' ?
			<Typography
				disableMargin
				preset={labelPreset}
				component='span'
				children={(children as any)}
			/> :
			children
	)

	if (variant === 'link') return (
		<a ref={(ref as LegacyRef<HTMLAnchorElement>)} href='#' {...buttonProps}>
			{child}
		</a>
	)

	return (
		<button ref={(ref as LegacyRef<HTMLButtonElement>)} type='button' {...buttonProps}>
			{child}
		</button>
	)
})

export default Button
