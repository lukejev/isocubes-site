import React from "react"
import makeStyles from "../../hooks/makeStyles"
import clsx from 'clsx'
import Theme from "../../context/Theme"

interface styleProps {
	contained?: boolean;
	width?: string;
	height?: string;
	center?: boolean;
}

const useStyles = makeStyles<styleProps>((theme) => ({
	base: {
		padding: "0",
		backgroundSize: ({ contained }) => contained ? "contain" : "cover",
		display: 'flex',
		justifyContent: ({ center }) => center && 'center',
		width: ({ width }) => width ? width : "auto",
		height: ({ height }) => height ? height : "auto",
	},
	wrapper: {
		display: 'flex',
		justifyContent: 'center',
		height: '100%',
		margin: 'auto'
	},
	image: {
		height: ({ height }) => height ? height : "100%",
		width: '100%',
		objectFit: 'contain',
		backgroundSize: ({ contained }) => contained ? "contain" : "cover",
	},

}), { name: 'Image' })

export interface ImageProps {
	style?: React.CSSProperties;
	[otherProps: string]: unknown;
	src: string;
	alt?: string;
	contained?: boolean;
	width?: string;
	height?: string;
	center?: boolean;
	wrapper?: boolean;
	className?: string
}

const Image: React.FC<ImageProps> = (props) => {
	const { 
		className: externalClasses,
		src, 
		alt, 
		contained, 
		width, 
		height,
		center,
		wrapper
	} = props

	const classes = useStyles({ contained, width, height, center })

	const className = clsx(
		classes.base,
		externalClasses
	)

	if (wrapper) {
		return (
			<div className={classes.wrapper}>
				<img src={src} alt={alt} className={classes.image} />
			</div>
		)
	}

	return (
		<img src={src} alt={alt} className={classes.base} />
	)
}

export default Image
