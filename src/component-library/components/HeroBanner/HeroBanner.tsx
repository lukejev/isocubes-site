import React from "react"
import clsx from 'clsx'
import makeStyles from '../../hooks/makeStyles'

interface styleProps {
	backgroundImage?: string
	palette: string
}

const useStyles = makeStyles<styleProps>(theme => ({
	herobanner: {
		display: "flex",
		alignItems: "center",
		padding: "0px 2rem",
		backgroundColor: theme.colors.blue,
		color: ({ palette }) => theme.getPalette(palette).textDark,
		backgroundImage: ({ backgroundImage }) => backgroundImage ? `url('${backgroundImage}')` : '',
		backgroundSize: "cover"
	},

	[theme.breakpoint.up('md') as any]: {
		herobanner: {
			padding: "0px 4rem",
		}
	},

	small: { height: "24em" },
	medium: { height: "28em" },
	large: { height: "42em" },

	left: {
		justifyContent: "flex-start",
		textAlign: "left"
	},
	center: {
		justifyContent: "center",
		textAlign: "center"
	},
	right: {
		justifyContent: "flex-end",
		textAlign: "right"
	},
}), { name: 'HeroBanner' })

export interface HeroBannerProps {
	title: string,
	subtitle?: string,
	body?: string,
	size?: "small" | "medium" | "large",
	alignment?: "left" | "center" | "right"
	backgroundImage?: string
	palette: string
}

const HeroBanner: React.FC<HeroBannerProps> = (props) => {
	const {
		title,
		subtitle,
		body,
		size,
		alignment,
		backgroundImage,
		palette = 'primary',
		children
	} = props

	const classes = useStyles({ backgroundImage, palette })

	const className = clsx(
		classes.herobanner,
		alignment === 'left' && classes.left,
		alignment === 'center' && classes.center,
		alignment === 'right' && classes.right,

		size === 'large' && classes.large,
		size === 'medium' && classes.medium,
		size === 'small' && classes.small,
	)

	const styleProps = {
		className
	}

	return (
		<div {...styleProps}>
			<div>
				<h1>{title}</h1>
				{subtitle && <h2>{subtitle}</h2>}
				{body && <p>{body}</p>}
				{ children }
			</div>
		</div>
	)
}

export default HeroBanner

