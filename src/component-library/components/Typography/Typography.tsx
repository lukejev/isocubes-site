import clsx from 'clsx'
import React from 'react'
import makeStyles from '../../hooks/makeStyles'

export interface TypographyProps {
	children?: React.ReactNode
	type?: 'heading' | 'paragraph' | 'label'
	preset?: 1 | 2 | 3 | 4
	emphasis?: 'lo' | 'mid' | 'high' | 'inherit'
	component?: string

	disableMargin?: boolean

	[otherProps: string]: unknown
}

const elements: { [type: string]: { [preset: number]: string } } = {
	paragraph: {
		1: 'p',
		2: 'p',
		3: 'p',
		4: 'p',
	},
	heading: {
		1: 'h1',
		2: 'h2',
		3: 'h3',
		4: 'h4',
	},
	label: {
		1: 'label',
		2: 'label',
		3: 'label',
		4: 'label',
	},
}

interface styleProps { margin: number | null }
const useStyles = makeStyles<styleProps>(theme => ({
	base: {
		fontFamily: theme.typography.paragraph,
		fontWeight: 400,
		margin: 0,
		marginBottom: ({ margin }) => margin ?? '.8rem'
	},

	heading: {
		fontFamily: theme.typography.heading,
		fontWeight: 600,
		margin: 0,
		marginBottom: ({ margin }) => margin ?? '1.2rem',
	},
	heading1: { fontSize: theme.spacing(5) },
	heading2: { fontSize: theme.spacing(4) },
	heading3: { fontSize: theme.spacing(3) },
	heading4: { fontSize: theme.spacing(2.5) },

	paragraph: {},
	paragraph1: { fontSize: theme.spacing(3) },
	paragraph2: { fontSize: theme.spacing(2.5) },
	paragraph3: { fontSize: theme.spacing(2) },
	paragraph4: { fontSize: theme.spacing(1.5) },

	label: {},
	label1: { fontSize: theme.spacing(2.5) },
	label2: { fontSize: theme.spacing(2) },
	label3: { fontSize: theme.spacing(1.5) },
	label4: { fontSize: theme.spacing(1) },

	emphasisLo: { color: 'inherit' },
	emphasisMid: { color: 'inherit' },
	emphasisHigh: { color: 'inherit' },
}), { name: 'Typography' })

const Typography: React.FC<TypographyProps> = (props) => {
	const {
		children,
		type = 'paragraph',
		preset = 3,
		emphasis = 'inherit',
		component,
		disableMargin = false,

		...otherProps
	} = props

	const Type = (component ?? elements[type][preset] as unknown) as React.FC<Record<string, unknown>>

	const presetClass = `${type}${preset}`
	const classes = useStyles({
		margin: disableMargin === true ? 0 : null
	})

	const classList = clsx(
		classes.base,

		type === 'heading' && classes.heading,
		type === 'paragraph' && classes.paragraph,
		type === 'label' && classes.label,
		classes[presetClass],

		emphasis === 'lo' && classes.emphasisLo,
		emphasis === 'mid' && classes.emphasisMid,
		emphasis === 'high' && classes.emphasisHigh,
	)

	return (
		<Type {...{...otherProps, children, className: classList}} />
	)
}

export default Typography
