import React from 'react'
import ReactDOM from 'react-dom'
import { useTheme } from 'react-jss'
import { ITheme } from '../../context/Theme'

export interface OffCanvasProps {
	children: React.ReactNode,
	className?: string
}

const OffCanvas: React.FC<OffCanvasProps> = (props) => {
	const {
		children,
		className
	} = props

	const { offCanvas } = useTheme<ITheme>()
	if (!offCanvas) return null
	return ReactDOM.createPortal(
		children,
		offCanvas,
		className
	)
}

export default OffCanvas
