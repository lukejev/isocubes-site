import React from 'react'
import clsx from 'clsx'
import { createUseStyles } from 'react-jss'
import { ITheme } from '../../context/Theme'

interface styleProps {
	center: boolean,
	marginX: number,
	marginY: number,
	paddingX: number,
	paddingY: number,
	palette: string
}

const useStyles: (...args: any[]) => any = createUseStyles<string, styleProps, ITheme>(theme => ({
	base: {
		marginLeft: ({ marginX, center }) => center ? 0 : theme.spacing(marginX),
		marginRight: ({ marginX, center }) => center ? 0 : theme.spacing(marginX),
		marginTop: ({ marginY }) => theme.spacing(marginY),
		marginBottom: ({ marginY }) => theme.spacing(marginY),
		paddingLeft: ({ paddingX }) => theme.spacing(paddingX),
		paddingRight: ({ paddingX }) => theme.spacing(paddingX),
		paddingTop: ({ paddingY }) => theme.spacing(paddingY),
		paddingBottom: ({ paddingY }) => theme.spacing(paddingY),
		backgroundColor: ({ palette }) => theme.getPalette(palette).backgroundLight
	},

	fullHeight: {
		height: '100vh'
	},

	altBg : {
		backgroundColor: ({ palette }) => theme.getPalette(palette).backgroundMain
	},

	center: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
	},

	alignCenter: {
		alignItems: 'center'
	}
}))

export interface ContainerProps {
	children: React.ReactNode;
	id?: string,
	center?: boolean,
	marginX?: number,
	marginY?: number,
	paddingX?: number,
	paddingY?: number,
	className?: string,
	palette: string,
	altBg?: boolean,
	fullHeight?: boolean,
	alignCenter?: boolean 
}

const Container: React.FC<ContainerProps> = (props) => {
	const {
		children,
		id,
		center,
		marginX,
		marginY,
		paddingX,
		paddingY,
		className: externalClasses,
		palette = 'primary',
		altBg,
		fullHeight,
		alignCenter
	} = props

	const classes = useStyles({ marginX, marginY, paddingX, paddingY, center, palette, fullHeight, alignCenter })

	const className = clsx(
		classes.base,
		center === true && classes.center,
		altBg === true && classes.altBg,
		fullHeight === true && classes.fullHeight,
		alignCenter === true && classes.alignCenter,
		externalClasses
	)

	return (
		<div id={id} className={clsx(className, classes.base)}>
			{children}
		</div>
	)
}

export default Container