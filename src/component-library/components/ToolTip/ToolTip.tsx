import React, { useEffect, useState } from 'react'
import makeStyles from '../../hooks/makeStyles'

import OffCanvas from '../OffCanvas'
import Card from '../Card'
import Typography from '../Typography'

interface Vec2 {
	x: number,
	y: number
}

type styleProps = Vec2

const useStyles = makeStyles<styleProps>(theme => ({
	card: {
		zIndex: theme.zIndex.context,
		padding: theme.spacing(.5, 1),
		position: 'fixed',
		top: ({ y }) => y,
		left: ({ x }) => x,
	}
}), { name: 'ToolTip' })

export interface ToolTipProps {
	target: React.RefObject<HTMLElement>
	elevation?: number
	children?: string
}

const ToolTip: React.FC<ToolTipProps> = (props) => {
	const {
		elevation = 1,

		target,
		children,
	} = props

	const [location, setLocation] = useState({ x: 0, y: 0 })

	const [active, setActive] = useState(false)
	const activate = () => setActive(true)
	const deactivate = () => setActive(false)
	const locate = (e: MouseEvent) => {setLocation({ x: e.clientX + 20, y: e.clientY - 6 }); console.log(e)}

	useEffect(() => {
		target.current?.addEventListener('mouseenter', activate)
		target.current?.addEventListener('mouseleave', deactivate)
		window.addEventListener('mousemove', locate)
		window.addEventListener('blur', deactivate)

		return () => {
			target.current?.removeEventListener('mouseenter', activate)
			target.current?.removeEventListener('mouseleave', deactivate)
			window.removeEventListener('mousemove', locate)
			window.removeEventListener('blur', deactivate)
		}
	}, [target.current])

	const classes = useStyles(location)

	if (!active) return null
	return (
		<OffCanvas>
			<Card className={classes.card} elevation={elevation}>
				<Typography variant='label' preset={4} disableMargin emphasis='mid'>
					{children}
				</Typography>
			</Card>
		</OffCanvas>
	)
}

export default ToolTip
