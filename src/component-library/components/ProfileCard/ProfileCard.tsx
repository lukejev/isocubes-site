import clsx from 'clsx'
import React from 'react'
import makeStyles from '../../hooks/makeStyles'

import Typography from '../Typography/Typography'
import Container from '../Container/Container'
import Image from '../Image/Image'
interface styleProps {
	image: string,
	alt: string,
	heading: string,
	description: string,
	dark: boolean
}

const useStyles = makeStyles<styleProps>(theme => ({
	base: {
		width: '100%',
		maxWidth: theme.spacing(40),
		backgroundColor: theme.colors.grey[100],
		color: theme.colors.black,
		borderRadius: theme.spacing(5),
		display: 'flex',
	},
	dark: {
		backgroundColor: theme.colors.grey[700],
		color: theme.colors.white
	},
	img: {
		width: theme.spacing(10),
		height: theme.spacing(10),
		margin: 'auto 0',
		marginRight: theme.spacing(2),
		borderRadius: '50%',
		backgroundColor: 'lightgray',
		objectFit: 'cover'
	},
	textWrapper: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center'

	},
	heading: {
		width: '90%',
		fontSize: theme.spacing(3)
	},
	description: {
		width: '90%'
	}
}), { name: 'Profile Card' })

export interface ProfileCardProps {
	children: React.ReactNode
	image: string,
	alt: string,
	heading: string,
	description: string,
	dark: boolean,
	className?: string
}

const ProfileCard: React.FC<ProfileCardProps> = (props) => {
	const {
		className: externalClasses,
		image,
		alt,
		heading,
		description,
		dark
	} = props

	const classes = useStyles({ image, alt, heading, description, dark })

	const className = clsx(
		classes.base,
		dark && classes.dark,
		externalClasses
	)

	return (
		<Container className={classes.base}>
			<Image className={classes.img} src={image} alt={alt} />
			<div className={classes.textWrapper}>
				<Typography type="heading" preset={4} disableMargin className={classes.heading}>{heading}</Typography>
				{description && 
					<Typography type="paragraph" disableMargin className={classes.description}>{description}</Typography>
				}
			</div> 
		</Container>
	)
}

export default ProfileCard
