import React, { useState, useEffect } from 'react'
import makeStyles from '../../hooks/makeStyles'

import Button from '../Button/Button'
import OffCanvas from '../OffCanvas'
import Card from '../Card'
import Typography from '../Typography'

const useStyles = makeStyles(theme => ({
	screen: {
		width: '100%',
		height: '100%',
		position: "absolute", 
		top: 0,
		left: 0,
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		background: theme.colors.backdrop[700]
	},
	card: {
		zIndex: theme.zIndex.context,
		padding: theme.spacing(.5, 1),
		position: 'fixed',
	}
}), { name: 'Lightbox' })


export interface LightboxProps {
	target: React.RefObject<HTMLElement>
	elevation?: number
	children?: string
}


const Lightbox: React.FC<LightboxProps> = (props) => {
	const {
		target,
		elevation = 2,
		children,
	} = props

	const [active, setActive] = useState(false)

	const activate = () => setActive(true)
	const deactivate = () => setActive(false)

	useEffect(() => {
		target.current?.addEventListener('click', activate)

		return () => {
			target.current?.removeEventListener('click', activate)
		}
	}, [target.current])

	const classes = useStyles()

	if (!active) return null
	return (
		<OffCanvas>
			<div className={classes.screen}>
				<Card className={classes.card} elevation={elevation}>
					<Typography variant='label' preset={4} disableMargin emphasis='mid'>
						{children}
						<Button onClick={() => deactivate()}>Close Lightbox</Button>
					</Typography>
				</Card>
			</div>
		</OffCanvas>
	)
}

export default Lightbox
