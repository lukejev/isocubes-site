import React from "react"
import { createUseStyles } from "react-jss"

interface styleProps {
	margin: string,
	width: string,
	minHeight: string,
	height: string
}

const useStyles: (...args: any[]) => any = createUseStyles<string, styleProps>(() => ({
	embed: {
		margin: "1em auto",
		width: "100%",
		minHeight: "500px",
		height: "100%",
	},
}))

export interface EmbedProps {
  url?: string;
  title?: string;
}

const Embed: React.FC<EmbedProps> = (props) => {
	const { url, title } = props

	const classes = useStyles()

	return (
		<iframe
			src={url}
			title={title}
			allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
			frameBorder="0"
			className={classes.embed}
		/>
	)
}

export default Embed

