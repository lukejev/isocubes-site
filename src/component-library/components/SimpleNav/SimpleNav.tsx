import React, { useState, useEffect, useMemo } from "react"

import makeStyles from "../../hooks/makeStyles"
import { useBreakpoint } from 'gatsby-plugin-breakpoints';
import clsx from 'clsx'

import Image from '../Image'
import Button from '../Button/Button'
import { ButtonProps } from "../Button"
import OffCanvas from '../OffCanvas'

import Collapse from '../Collapse'
import Typography from "../Typography/Typography"

const useStyles = makeStyles(theme => ({
	SimpleNav: {
		boxSizing: "border-box",
		display: "flex",
		width: "100%",
		justifyContent: "space-between",
		alignItems: "center",
		padding: "0px 2rem",
		height: "80px",
		backgroundColor: theme.colors.grey[50],
		position: 'relative',
		color: theme.colors.grey[100]
	},

	SimpleNavMobile: {
		padding: "0px 4rem"
	},

	linksWrapper: {
		display: 'flex',
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},

	linksWrapperMobile: {
		flexDirection: 'column'
	},

	link: {
		margin: '16px',
		textDecoration: 'none',
		color: theme.colors.white
	},

	linkMobile: {
		margin: '8px',
	},

	svg: {
		fontSize: '40px',
		margin: '16px'
	},

	mobileMenu: {
		width: '50%',
		position: 'absolute',
		top: 0
	},

	DropDown: {
		background: theme.palettes.primary.backgroundLight,
		display: "flex",
		flexDirection: "row",
		boxSizing: "border-box",
		alignItems: "flex-start",
		transition: "all 0.3s",
		overflow: "hidden",
		padding: "1em",
	},
}), { name: 'SimpleNav' })

export interface SimpleNavProps {
	title: string,
	logoImage?: string,
	links?: ButtonProps[]
	cta?: ButtonProps
}

const SimpleNav: React.FC<SimpleNavProps> = (props) => {
	const {
		title,
		logoImage,
		links,
		children,
		cta
	} = props

	const breakpoints = useBreakpoint();

	const [mobileNavActive, setMobileNavActive] = useState<boolean>(false) 

	const navClick = () => {
		setMobileNavActive(!mobileNavActive)
	}

	const navItemClick = () => {
		setMobileNavActive(false)
	}

	const classes = useStyles({})

	const navContent = useMemo(() => (
		<>
			<div className={clsx(classes.linksWrapper, breakpoints.sm && classes.linksWrapperMobile)}>
				{links && links.map((link, i) => 
					(<a onClick={navItemClick} className={clsx(classes.link, breakpoints.sm && classes.linkMobile)} href={link.href} key={i}>
						{link.children}
					</a>))}
				{cta && <Button {...cta} />}
				{ children }
			</div>
		</>
	), [links, cta, breakpoints.sm])

	return (
		breakpoints.sm ? 
			<div className={clsx(classes.SimpleNav, classes.SimpleNavMobile)}>
				{logoImage ? <Image src={logoImage} width="auto" height="62px" contained style={{ padding: "10px" }} /> : <h1>{title}</h1>}
				<div>
					<Button onClick={navClick}>{mobileNavActive ? "X" : "Menu"}</Button>
					<OffCanvas>
						<div className={classes.mobileMenu}>
							<Collapse duration={.3} active={mobileNavActive}>
								<nav className={classes.DropDown}>
									{navContent}
								</nav>
							</Collapse>
						</div>
					</OffCanvas>
				</div>
			</div>
			: 
			<div className={classes.SimpleNav}>
				{logoImage ? <Image src={logoImage} width="auto" height="62px" contained style={{ padding: "10px" }} /> : <h1>{title}</h1>}
				<nav>
					{navContent}
				</nav>
			</div>
	)
}

export default SimpleNav
