import clsx from 'clsx'
import React, { createRef, useEffect } from 'react'
import { useTheme } from 'react-jss'
import { ITheme, TransitionConfig } from '../../context/Theme'
import makeStyles from '../../hooks/makeStyles'

interface styleProps {
	timing: TransitionConfig | string
}

const useStyles = makeStyles<styleProps>(theme => ({
	base: {
		transition: ({ timing }) => (
			typeof timing === 'string' ?
				timing :
				theme.transition.custom(timing)
		),
	}
}), { name: 'Animation' })

type AnimationHandler = (event: TransitionEvent) => void

export interface AnimationProps {
	children: React.ReactNode
	className?: string
	method?: string
	duration?: number
	delay?: number
	onStart?: AnimationHandler
	onEnd?: AnimationHandler
	onCancel?: AnimationHandler
	onFinal?: AnimationHandler
	active?: boolean,
	baseClass: string,
	activeClass: string
}

const Animation: React.FC<AnimationProps> = (props) => {
	const theme = useTheme<ITheme>()

	const {
		children,
		className: externalClasses,
		onStart,
		onEnd,
		onCancel,
		onFinal,
		active,
		baseClass,
		activeClass,

		method = theme.transition.easeInOut,
		duration = 0.15,
		delay,
	} = props

	const animationRef = createRef<HTMLDivElement>()

	const handleStart = (e: TransitionEvent) => {
		if (typeof onStart === 'function') {
			onStart(e)
		}
	}

	const handleEnd = (e: TransitionEvent) => {
		if (typeof onEnd === 'function') {
			onEnd(e)
		}
		if (typeof onFinal === 'function') {
			onFinal(e)
		}
	}

	const handleCancel = (e: TransitionEvent) => {
		if (typeof onCancel === 'function') {
			onCancel(e)
		}
		if (typeof onFinal === 'function') {
			onFinal(e)
		}
	}

	useEffect(() => {
		animationRef.current?.addEventListener('transitionstart', handleStart)
		animationRef.current?.addEventListener('transitionend', handleEnd)
		animationRef.current?.addEventListener('transitioncancel', handleCancel)
		return () => {
			animationRef.current?.removeEventListener('transitionstart', handleStart)
			animationRef.current?.removeEventListener('transitionend', handleEnd)
			animationRef.current?.removeEventListener('transitioncancel', handleCancel)
		}

	}, [])

	const timing = { method, duration, delay }
	const classes = useStyles({ timing })

	const className = clsx(
		classes.base,
		externalClasses,
		baseClass,
		active && activeClass
	)

	//	TODO: provide props to change transition values 
	return (
		<div className={className} ref={animationRef}>
			{children}
		</div>
	)
}

export default Animation
