import React from 'react'
import clsx from 'clsx'
import makeStyles from '../../hooks/makeStyles'
import { FaBeer } from 'react-icons/fa'
import Typography from '../Typography'
interface styleProps {
	dark: boolean
}

const useStyles = makeStyles<styleProps>(theme => ({
	base: {
		display: 'flex',
		flexDirection: 'row',
		width: 'auto',
		minWidth: 'unset',
		margin: 'auto',
		position: 'unset',
		justifyContent: 'space-between',
		padding: theme.spacing(1),
		borderRadius: theme.spacing(1),
		boxShadow: theme.elevation[1],
	},
	light: {
		backgroundColor: theme.colors.white,
		color: theme.colors.black,
		border: `${theme.spacing(0.125)} solid ${theme.colors.grey[700]}`
	},
	dark: {
		backgroundColor: theme.colors.grey[700],
		color: theme.colors.white
	},
	right: {
		right: theme.spacing(4)
	},
	left: {
		left: theme.spacing(4)
	},

	content: {
		display: 'flex',
		alignItems: 'center',
		width: 'auto',
		flexShrink: 1,
		marginRight: theme.spacing(2),
	},
	btnWrapper: {
		display: 'flex',
		alignItems: 'center',
		flexShrink: 0,
	},

	[theme.breakpoint.up('md') as any]: {
		base: {
			width: 'fit-content',
			minWidth: theme.spacing(40),
			maxWidth: theme.spacing(90),
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'space-around',
			position: 'absolute',
			top: theme.spacing(2)
		},
		btnWrapper: {
			marginLeft: theme.spacing(2)
		}
	}
}), { name: 'Toast' })

export interface ToastProps {
	children: React.ReactNode;
	dark: boolean;
	text: string;
	position: string;
	actions?: React.ReactNode;
	className?: string
}

const Toast: React.FC<ToastProps> = (props) => {

	const {
		dark,
		text,
		position,
		actions,
		className: externalClasses,
	} = props

	const classes = useStyles()

	const className = clsx(
		classes.base,
		dark === true ? classes.dark : classes.light,
		position === 'right' ? classes.right : classes.left,
		externalClasses
	)

	return (
		<div className={clsx(className, classes.base)}>
			<div className={classes.content}>
				<Typography disableMargin>{text} <FaBeer /></Typography>
			</div>
			<div className={classes.btnWrapper}>
				{actions}
			</div>
		</div>
	)
}

export default Toast