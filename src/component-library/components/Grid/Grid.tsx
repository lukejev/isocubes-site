import React from 'react'
import { createUseStyles } from "react-jss"
import { ITheme } from '../../context/Theme'

const useStyles = createUseStyles<string, unknown, ITheme>(theme => ({
	Grid: {
		display: "grid",
		gridTemplateColumns: "repeat(12, 1fr)",
		gridTemplateRows: "repeat(12, 1fr)",
		gridGap: "1.25em",
		height: "100vh",
		padding: "1.25em",
		background: theme.colors.grey[900]
	},
}))

export interface GridProps {
	children?: React.ReactNode
}

const Grid: React.FC<GridProps> = (props) => {

	const classes = useStyles()

	return (
		<div className={classes.Grid}>
			{props.children}
		</div>
	)
}

export default Grid