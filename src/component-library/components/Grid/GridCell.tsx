import React from 'react'
import { createUseStyles } from "react-jss"
import { ITheme } from '../../context/Theme'


interface styleProps {
	rows: string
	columns: string
}


const useStyles = createUseStyles<string, styleProps, ITheme>(theme => ({
	GridCell: {
		display: "flex",
		flexDirection: "column",
		flex: "auto",
		background: theme.colors.white,
		borderRadius: "4px",
		gridArea: ({ rows, columns }) => `auto / auto / span ${rows} / span ${columns}`
	},
}))

export interface GridCellProps {
	children?: React.ReactNode
	key?: number,
	columns: string,
	rows: string
}

const GridCell: React.FC<GridCellProps> = (props) => {

	const { rows, columns, children } = props

	const classes = useStyles({ rows, columns })

	return (
		<div className={classes.GridCell}>
			{children}
		</div>
	)
}

export default GridCell